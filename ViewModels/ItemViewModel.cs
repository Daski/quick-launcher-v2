﻿using System;
using Quick_Launcher.Models;
using System.Windows.Input;
using Quick_Launcher.Validation;
using System.Windows;
using Quick_Launcher.Helpers;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Quick_Launcher.ViewModels
{
	public class ItemViewModel
	{
		private readonly ItemValidator Validator;
		private readonly ApplicationItem SelectedItem;
		private readonly ObservableCollection<ApplicationItem> Items;

		private string _name;
		public string Name
		{
			get => _name;
			set => _name = value;
		}

		private string _path;
		public string Path
		{
			get => _path;
			set => _path = value;
		}

		private string _group;
		public string Group
		{
			get => _group;
			set => _group = value;
		}

		private bool _isAdminMode;
		public bool IsAdminMode
		{
			get => _isAdminMode;
			set => _isAdminMode = value;
		}

		private ICommand _addCommand;
		public ICommand Add => _addCommand ?? (_addCommand = new CommandHandler(param => AddExecute(), true));

		private ICommand _closeCommand;
		public ICommand Close => _closeCommand ?? (_closeCommand = new RelayCommand<Window>(param => CloseExecute(param)));

		public ItemViewModel()
		{
			Items = ApplicationsManager.Instance.Applications;
		}

		public ItemViewModel(ApplicationItem item)
		{
			SelectedItem = item;
			Items = ApplicationsManager.Instance.Applications;

			Name = SelectedItem.Name;
			Path = SelectedItem.Path;
			Group = SelectedItem.Group;
			IsAdminMode = SelectedItem.IsAdminMode;
		}

		public void AddExecute()
		{
			var item = new ApplicationItem
			{
				Name = _name,
				Path = _path,
				Group = _group,
				IsAdminMode = _isAdminMode
			};

			var errors = new List<string>();
			if (Validate(item, errors))
			{
				Items.Add(item);
				Application.Current.MainWindow.Close();
			}
		}

		private void CloseExecute(Window window)
		{
			if (window != null)
			{
				window.Close();
			}
		}

		public bool IsIsExistsPathIcon { get; set; }

		private bool Validate(ApplicationItem newItem, IList<string> errors)
		{
			var validation = new ItemValidator();

			if (SelectedItem != null)
			{
				if(newItem.Name != SelectedItem.Name && !validation.IsUniqueName(newItem.Name))
				{
					errors.Add("Name is not unique");
					Console.WriteLine("Name is not unique");
					return false;
				}
			}
			else
			{
				
				if (!validation.IsUniqueName(newItem.Name))
				{
					errors.Add("Name is not unique");
					Console.WriteLine("Name is not unique");
					return false;
				}
			}

			if (!validation.IsExistsPath(newItem.Path))
			{
				errors.Add("Path is not unique");
				Console.WriteLine("Path is not unique");
				return false;
			}

			return true;
		}

	}
}
