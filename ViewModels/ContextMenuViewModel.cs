﻿using Daski.Extensions.String;
using Quick_Launcher.Commands;
using Quick_Launcher.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Controls;
using System.Windows.Input;

namespace Quick_Launcher.ViewModels
{
	public class ContextMenuViewModel
	{
		private ContextMenuCommands _commands;
		private ObservableCollection<ApplicationItem> _applicationItems;
		public ObservableCollection<MenuItem> MenuItems { get; private set; }

		public ContextMenuViewModel()
		{
			_applicationItems = ApplicationsManager.Instance.Applications;
			_commands = new ContextMenuCommands();
			MenuItems = new ObservableCollection<MenuItem>();
			UpdateContextMenu();
		}

		public void UpdateContextMenu()
		{
			AddMenuItem("Configuration", _commands.ShowConfiguration);
			var groups = GetGroupsNames();
			if (groups.Count > 0)
			{
				PrepareGroupsMenu(groups);
			}

			PrepareApplicationsMenu();
			AddMenuItem("Exit", _commands.ShutDownApp);
		}

		private HashSet<string> GetGroupsNames()
		{
			var groupNames = new HashSet<string>();
			foreach (var application in _applicationItems)
			{
				if (!application.Group.IsNullOrEmpty())
				{
					groupNames.Add(application.Group);
				}
			}

			return groupNames;
		}

		private void PrepareGroupsMenu(HashSet<string> groupsNames)
		{
			foreach (var group in groupsNames)
			{
				var menuItem = new MenuItem();
				menuItem.Header = group;

				foreach (var application in _applicationItems)
				{
					if (!application.Group.IsNullOrEmpty() && application.Group.Equals(group))
					{
						var subMenuItem = new MenuItem();
						subMenuItem.Header = application.Name;
						subMenuItem.Command = _commands.RunApplciation;
						subMenuItem.CommandParameter = application.Name;

						menuItem.Items.Add(subMenuItem);
					}
				}

				MenuItems.Add(menuItem);
			}
		}

		private void PrepareApplicationsMenu()
		{
			foreach (var application in _applicationItems)
			{
				if (application.Group.IsNullOrEmpty())
				{
					AddMenuItem(application.Name, _commands.RunApplciation);
				}
			}
		}

		private void AddMenuItem(string name, ICommand command)
		{
			var menuItem = new MenuItem
			{
				Header = name,
				Command = command,
				CommandParameter = name
			};

			MenuItems.Add(menuItem);
		}
	}
}




