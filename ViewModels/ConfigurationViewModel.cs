﻿using Quick_Launcher.Helpers;
using Quick_Launcher.Models;
using Quick_Launcher.Views;
using System;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Input;

namespace Quick_Launcher.ViewModels
{
	public class ConfigurationViewModel
	{
		public ObservableCollection<ApplicationItem> Applications { get; private set; }
		public ApplicationItem SelectedAppItem { get; set; }

		private ICommand _addCommand;
		private ICommand _removeCommand;
		private ICommand _closeCommand;
		private ICommand _editCommand;

		public ICommand Add => _addCommand ?? (_addCommand = new CommandHandler(param => AddExecute(), true));
		public ICommand Remove => _removeCommand ?? (_removeCommand = new CommandHandler(param => RemoveExecute(), true));
		public ICommand Close => _closeCommand ?? (_closeCommand = new RelayCommand<Window>(param => CloseExecute(param)));
		public ICommand Edit => _editCommand ?? (_editCommand = new CommandHandler(param => EditExecute(), true));

		public ConfigurationViewModel()
		{
			Applications = ApplicationsManager.Instance.Applications;
		}

		private void AddExecute()
		{
			var itemView = new ItemView();
			itemView.ShowDialog();
		}

		private void RemoveExecute()
		{
			if (SelectedAppItem != null)
			{
				Applications.Remove(SelectedAppItem);
			}
		}

		private void CloseExecute(Window window)
		{
			if (window != null)
			{
				window.Close();
			}
		}

		public void EditExecute()
		{
			if (SelectedAppItem != null)
			{
				var itemView = new ItemView();
				itemView.DataContext = new ItemViewModel(SelectedAppItem);
				itemView.ShowDialog();
			}
		}
	}
}
