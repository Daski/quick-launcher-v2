﻿using System;
using System.Collections.ObjectModel;
using Quick_Launcher.Helpers;
using Quick_Launcher.Models;

namespace Quick_Launcher.ViewModels
{
	public sealed class ApplicationsManager : Singleton<ApplicationsManager>
	{
		public ObservableCollection<ApplicationItem> Applications { get; set; }

		private ApplicationsManager()
		{
			Applications = new ObservableCollection<ApplicationItem>();
			Applications.Add(new ApplicationItem { Name = "name 1", Path = "path 1", Group = "group 1" });
			Applications.Add(new ApplicationItem { Name = "name 2", Path = "path 2", Group = "group 1" });
			Applications.Add(new ApplicationItem { Name = "name 3", Path = "path 3", Group = "group 1" });
			Applications.Add(new ApplicationItem { Name = "name 4", Path = "path 4", Group = "group 2" });
			Applications.Add(new ApplicationItem { Name = "name 5", Path = "path 5", Group = "group 2" });
			Applications.Add(new ApplicationItem { Name = "name 6", Path = "path 6", Group = "group 2", IsAdminMode = true });
			Applications.Add(new ApplicationItem { Name = "name 7", Path = "path 7"});


		}
	}
}
