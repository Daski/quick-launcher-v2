﻿using Quick_Launcher.Models;
using Quick_Launcher.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace Quick_Launcher.Validation
{
	public class ItemValidator
	{
		private readonly ObservableCollection<ApplicationItem> Applications;

		public ItemValidator()
		{
			Applications = ApplicationsManager.Instance.Applications;
		}

		public bool IsUniqueName(string name)
		{
			return !Applications.Any(i => i.Name.Equals(name));
		}

		public bool IsExistsPath(string path)
		{
			return File.Exists(path);
		}

		public bool IsExistsGroup(string group)
		{
			if(!string.IsNullOrEmpty(group))
			{
				return Applications.Any(i => i.Group.Equals(group));
			}

			return true;
		}
	}
}