﻿using Quick_Launcher.Models;
using System;
using System.Windows.Input;

namespace Quick_Launcher.Commands
{
	public class ApplicationItemEditorCommands
	{
		public ApplicationItemEditorCommands()
		{

		}

		private ICommand _addCommand;
		public ICommand Add => _addCommand ?? (_addCommand = new CommandHandler(param => AddExecute(), true));

	}
}
