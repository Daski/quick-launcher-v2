﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using Quick_Launcher.Helpers;
using Quick_Launcher.Models;
using Quick_Launcher.ViewModels;
using Quick_Launcher.Views;

namespace Quick_Launcher.Commands
{
	public class ContextMenuCommands
	{
		private ConfigurationView _configurationView;

		private ICommand _showConfiguration;
		private ICommand _shutDownApp;
		private ICommand _runApplication;

		public ICommand ShowConfiguration => _showConfiguration ?? (_showConfiguration = new CommandHandler(param => ShowConfigurationExecute(), true));
		public ICommand ShutDownApp => _shutDownApp ?? (_shutDownApp = new CommandHandler(param => ShutDownAppExecute(), true));
		public ICommand RunApplciation => _runApplication ?? (_runApplication = new CommandHandler(param => RunApplicationExecute(param), true));

		public void OnConfigurationWindowClosed(object source, EventArgs args)
		{
			_configurationView = null;
		}

		private void ShowConfigurationExecute()
		{
			if (_configurationView == null)
			{
				_configurationView = new ConfigurationView();
				_configurationView.Closed += OnConfigurationWindowClosed;
				_configurationView.ShowDialog();
			}
		}

		private void ShutDownAppExecute()
		{
			Application.Current.Shutdown();
		}

		private void RunApplicationExecute(object parameter)
		{
			var name = parameter as string;
			ApplicationItem applicationItem = null;

			foreach (var item in ApplicationsManager.Instance.Applications)
			{
				if (item.Name == name)
				{
					applicationItem = item;
				}
			}

			if (applicationItem != null)
			{
				using (var process = new Process())
				{
					if (applicationItem.IsAdminMode)
					{
						process.StartInfo.UseShellExecute = true;
						process.StartInfo.Verb = "runas";
					}

					process.StartInfo.FileName = applicationItem.Path;

					try
					{
						process.Start();
					}
					catch (Win32Exception ex)
					{
						if (ex.NativeErrorCode == 1223)
						{
							MessageBox.Show("Operation canceled by user");
						}
						else
						{
							throw;
						}
					}
				}
			}
		}





	}
}
