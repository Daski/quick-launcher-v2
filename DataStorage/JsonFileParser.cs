﻿using Newtonsoft.Json;
using Quick_Launcher.Models;
using Quick_Launcher.ViewModels;
using System;
using System.Collections.ObjectModel;
using System.IO;

namespace Quick_Launcher.DataStorage
{
	public class DataStorage : IDataStorage
	{
		private readonly string filePath;
		private ObservableCollection<ApplicationItem> _applications;

		public DataStorage(string filePath)
		{
			_applications = ApplicationsManager.Instance.Applications;
			filePath = @"d:\Development\Repositories\application.json";
		}

		public void Save()
		{
			var jsonFile = JsonConvert.SerializeObject(_applications);
			using (var writer = new StreamWriter(filePath))
			{
				writer.Write(jsonFile);
			}
		}

		public void Load()
		{
			var fileStream = File.ReadAllText(filePath);
			_applications = JsonConvert.DeserializeObject<ObservableCollection<ApplicationItem>>(fileStream);
		}
	}
}
