﻿using System;
using System.Reflection;

namespace Quick_Launcher.Helpers
{
	public class Singleton<T> where T : class
	{
		private static volatile T _instance;
		private static readonly object syncRoot = new object();

		public static T Instance
		{
			get
			{
				if (_instance == null)
				{
					lock (syncRoot)
					{
						if (_instance == null)
						{
							ConstructorInfo constructor = null;

							try
							{
								constructor = typeof(T).GetConstructor(BindingFlags.Instance | BindingFlags.NonPublic, null, new Type[0], null);
							}
							catch(Exception ex)
							{
								throw;
							}

							if(constructor == null || constructor.IsAssembly)
							{
								//error
							}

							_instance = (T)constructor.Invoke(null);
						}
					}
				}

				return _instance;
			}
		}
	}
}
