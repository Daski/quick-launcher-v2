﻿using System;

namespace Quick_Launcher.Models
{
	public class ApplicationItem
	{
		public string Name { get; set; }
		public string Path { get; set; }
		public string Group { get; set; }
		public bool IsAdminMode { get; set; }
	}
}
